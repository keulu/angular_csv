import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { File as FileCsv } from './file';


@Injectable({ providedIn: 'root' })
export class FileService {

  private filesUrl = 'http://localhost:8080/api/file';  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient) { }

  /** GET files from the server */
  getFiles(): Observable<FileCsv[]> {
    return this.http.get<FileCsv[]>(this.filesUrl)
      .pipe(
        tap(_ => this.log('fetched files')),
        catchError(this.handleError<FileCsv[]>('getFiles', []))
      );
  }

  /** GET file by id. Return `undefined` when id not found */
  getFileNo404<Data>(id: number): Observable<FileCsv> {
    const url = `${this.filesUrl}/?id=${id}`;
    return this.http.get<FileCsv[]>(url)
      .pipe(
        map(files => files[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? 'fetched' : 'did not find';
          this.log(`${outcome} file id=${id}`);
        }),
        catchError(this.handleError<FileCsv>(`getFile id=${id}`))
      );
  }

  /** GET file by id. Will 404 if id not found */
  getFile(id: number): Observable<FileCsv> {
    const url = `${this.filesUrl}/${id}`;
    return this.http.get<FileCsv>(url).pipe(
      tap(_ => this.log(`fetched file id=${id}`)),
      catchError(this.handleError<FileCsv>(`getFile id=${id}`))
    );
  }

  /* GET files whose name contains search term */
  searchFiles(term: string): Observable<FileCsv[]> {
    if (!term.trim()) {
      // if not search term, return empty file array.
      return of([]);
    }
    return this.http.get<FileCsv[]>(`${this.filesUrl}/?name=${term}`).pipe(
      tap(x => x.length ?
         this.log(`found files matching "${term}"`) :
         this.log(`no files matching "${term}"`)),
      catchError(this.handleError<FileCsv[]>('searchFiles', []))
    );
  }

  //////// Save methods //////////

  /** POST: add a new file to the server */
  addFile(file:FormData, httpOptions={}): Observable<Object> {
    return this.http.post(this.filesUrl, file, httpOptions);
  }

  /** DELETE: delete the file from the server */
  deleteFile(id: number): Observable<FileCsv> {
    const url = `${this.filesUrl}/${id}`;

    return this.http.delete<FileCsv>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted file id=${id}`)),
      catchError(this.handleError<FileCsv>('deleteFile'))
    );
  }

  /** PUT: update the file on the server */
  updateFile(file: FileCsv): Observable<any> {
    const url = `${this.filesUrl}/${file.id}`;

    return this.http.put(url, file, this.httpOptions).pipe(
      tap(_ => this.log(`updated file id=${file.id}`)),
      catchError(this.handleError<any>('updateFile'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   *
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a FileCsvService message with the MessageService */
  private log(message: string) {
    // this.messageService.add(`FileCsvService: ${message}`);
  }
}
