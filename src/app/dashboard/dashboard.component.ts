import { Component, OnInit } from '@angular/core';
import { HttpEventType } from '@angular/common/http';

import { FileService } from '../file.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.scss' ]
})
export class DashboardComponent {
  

    fileName = '';
    uploadProgress:number = 0;
    
    constructor(private fileService: FileService) {
    }

    onFileSelected(event: any) {
      const file:File = event.target.files[0];
    
      if (file.name) {
        const formData = new FormData();
        formData.append("file", file);
        formData.append("test", "coucou");
        formData.append("allo", "test");

        this.fileService.addFile(formData, {
          reportProgress: true,
          observe: 'events',
          headers: { 'Content-Type': 'multipart/form-data; boundary=---' }
        }).subscribe(data => {
          console.log(data);
        });

        // let uploadSub = upload$.subscribe(newEvent => {
        //   if (newEvent.type == HttpEventType.UploadProgress) {
        //     this.uploadProgress = Math.round(100 * (newEvent.loaded / newEvent.total));
        //   }
        // })

      } 
    }

  
}
