export interface File {
    id: number;
    band_name: string;
    country: string;
    city: string;
    year_start: number;
    year_stop: number;
    creators: string;
    style: string;
    nb_members: number;
    description: string;
}
  