import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { File } from '../file';
import { FileService } from '../file.service';

@Component({
  selector: 'app-file-detail',
  templateUrl: './file-detail.component.html',
  styleUrls: [ './file-detail.component.scss' ]
})
export class FileDetailComponent implements OnInit {
  file: File | undefined;

  constructor(
    private route: ActivatedRoute,
    private fileService: FileService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getFile();
  }

  getFile(): void {
    const id = parseInt(this.route.snapshot.paramMap.get('id')!, 10);
    this.fileService.getFile(id)
      .subscribe(file => this.file = file);
  }

  delete(file: File): void {
    if (confirm('sure de vous ?')) {
      this.fileService.deleteFile(file.id)
        .subscribe(() => this.goBack());
    }
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    if (this.file) {
      this.fileService.updateFile(this.file)
        .subscribe(() => this.goBack());
    }
  }
}
